'use strict';

require('./db_connection');

const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    filmsModule = require('./routes/films/index.js'),
    shopModule = require('./routes/shop/index.js');

app.set('view engine', 'pug');

app.use(morgan('dev'));
app.use(bodyParser.json());

app.get('/', (req, res) => res.render('index'));

app.use('/doc', express.static('./apidoc'));

app.use('/films', filmsModule);

app.use('/shop', shopModule);

app.listen(8082);