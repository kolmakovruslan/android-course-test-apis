#!/bin/sh

DOC_PATH=./apidoc
CODE_PATH=./routes/
API_DOC_PATH=./node_modules/apidoc/bin/apidoc

rm -rf ${DOC_PATH}
node ${API_DOC_PATH} -i ${CODE_PATH} -o ${DOC_PATH}