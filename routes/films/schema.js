'use strict';

/**
 * @apiDefine FilmContent
 * @apiParam {String} name имя
 * @apiParam {String} genre жанр
 * @apiParam {String} description описание
 * @apiParam {String} picture ссылка на изображение
 * @apiParam {String} country страна
 * @apiParam {Number} year год
 */

/**
 * @apiDefine FilmId
 * @apiParam {String} _id идентификатор фильма
 */

/**
 * @apiDefine FilmsResult
 * @apiSuccess {Object[]} film список фильмов
 * @apiSuccess {String} film._id идентификатор
 * @apiSuccess {String} film.genre жанр
 * @apiSuccess {String} film.description описание
 * @apiSuccess {String} film.picture ссылка на изображение
 * @apiSuccess {String} film.country страна
 * @apiSuccess {String} film.year год
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FilmSchema = new Schema({
    _id: {type: String, default: "NoId"},
    name: {type: String, default: "Empty name"},
    genre: {type: String, default: "Empty genre"},
    description: {type: String, default: "Empty description"},
    picture: {type: String, default: "Empty picture link"},
    country: {type: String, default: "Empty country"},
    year: {type: Number, default: 0},
});

module.exports.FilmSchema = FilmSchema;