'use strict';

/**
 * @apiDefine Films Фильмотека
 */

const express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    uuid = require('uuid/v4'),
    common = require('../../common'),
    defaultResultResponse = common.defaultResultResponse,
    defaultPutResponse = common.defaultPutResponse,
    defaultPostResponse = common.defaultPostResponse,
    defaultDeleteResponse = common.defaultDeleteResponse,
    defaultErrorResponse = common.defaultErrorResponse,
    FilmSchema = require('./schema').FilmSchema;

const Film = mongoose.model("Film", FilmSchema);

/**
 * @api {get} /films список
 * @apiVersion 0.0.1
 * @apiGroup Films
 * @apiUse FilmsResult
 */
router.get('/', (req, res) => {
    Film.find()
        .exec()
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {put} /films добавление
 * @apiVersion 0.0.1
 * @apiGroup Films
 * @apiUse FilmContent
 */
router.put('/', (req, res) => {
    Film.create(Object.assign(req.body, {_id: uuid()}))
        .then(defaultPutResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {post} /films обновление
 * @apiVersion 0.0.1
 * @apiGroup Films
 * @apiUse FilmId
 * @apiUse FilmContent
 */
router.post('/', (req, res) => {
    Film.update(req.body)
        .then(defaultPostResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {delete} /films удаление
 * @apiVersion 0.0.1
 * @apiGroup Films
 * @apiUse FilmId
 */
router.delete('/', (req, res) => {
    Film.remove({_id: req.body._id})
        .then(defaultDeleteResponse(res))
        .catch(defaultErrorResponse(res))
});

module.exports = router;