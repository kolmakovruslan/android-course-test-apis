'use strict';

/**
 * @apiDefine Shops Магазин (Магазины)
 */
/**
 * @apiDefine ShopsUsers Магазин (Пользователи)
 */
/**
 * @apiDefine ShopsCategories Магазин (Категории)
 */
/**
 * @apiDefine ShopsProducts Магазин (Товары)
 */
/**
 * @apiDefine ShopsOrders Магазин (Заказы)
 */

const express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    uuid = require('uuid/v4'),
    common = require('../../common'),
    defaultResultResponse = common.defaultResultResponse,
    defaultPutResponse = common.defaultPutResponse,
    defaultPostResponse = common.defaultPostResponse,
    defaultDeleteResponse = common.defaultDeleteResponse,
    defaultErrorResponse = common.defaultErrorResponse,
    ShopSchema = require('./schema').ShopSchema,
    UserSchema = require('./schema').UserSchema,
    CategorySchema = require('./schema').CategorySchema,
    ProductSchema = require('./schema').ProductSchema,
    OrderSchema = require('./schema').OrderShcema;

const Shop = mongoose.model("Shop", ShopSchema);
const User = mongoose.model("User", UserSchema);
const Category = mongoose.model("Category", CategorySchema);
const Product = mongoose.model("Product", ProductSchema);
const Order = mongoose.model("Order", OrderSchema);

/**
 * @api {put} /shop добавление
 * @apiVersion 0.0.1
 * @apiGroup Shops
 * @apiUse ShopContent
 * @apiUse ShopInfo
 */
router.put('/', (req, res) => {
    Shop.create(Object.assign(req.body, {_id: uuid()}))
        .then(defaultPutResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop список
 * @apiVersion 0.0.1
 * @apiGroup Shops
 * @apiUse ShopList
 */
router.get('/', (req, res) => {
    Shop.find()
        .exec()
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {put} /shop/users/:shopId/ добавление
 * @apiVersion 0.0.1
 * @apiGroup ShopsUsers
 * @apiUse UserContent
 * @apiUse UserInfo
 */
router.put('/users/:shopId', (req, res) => {
    User.create(Object.assign(req.body, {_id: uuid(), shopId: req.params.shopId}))
        .then(defaultPutResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop/users/:shopId/ список
 * @apiVersion 0.0.1
 * @apiGroup ShopsUsers
 * @apiUse UserList
 */
router.get('/users/:shopId', (req, res) => {
    User.find({shopId: req.params.shopId})
        .exec()
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop/users/check/:login/ получить по логину
 * @apiVersion 0.0.1
 * @apiGroup ShopsUsers
 * @apiUse UserInfo
 */
router.get('/users/check/:login', (req, res) => {
    User.findOne({login: req.params.login})
        .exec()
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {post} /shop/users/:userId/ обновление
 * @apiVersion 0.0.1
 * @apiGroup ShopsUsers
 * @apiUse UserContent
 * @apiUse UserInfo
 */
router.post('/users/:userId', (req, res) => {
    User.update(Object.assign(req.body, {_id: req.params.userId}))
        .then(defaultPostResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {put} /shop/categories/:shopId/ добавление
 * @apiVersion 0.0.1
 * @apiGroup ShopsCategories
 * @apiUse CategoryContent
 * @apiUse CategoryInfo
 */
router.put('/categories/:shopId', (req, res) => {
    Category.create(Object.assign(req.body, {_id: uuid(), shopId: req.params.shopId}))
        .then(defaultPutResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop/categories/:shopId список
 * @apiVersion 0.0.1
 * @apiGroup ShopsCategories
 * @apiUse CategoryContent
 * @apiUse CategoryList
 */
router.get('/categories/:shopId', (req, res) => {
    Category.find({shopId: req.params.shopId})
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {put} /shop/products/:shopId добавление
 * @apiVersion 0.0.1
 * @apiGroup ShopsProducts
 * @apiUse ProductContent
 * @apiUse ProductInfo
 */
router.put('/products/:shopId', (req, res) => {
    Product.create(Object.assign(req.body, {_id: uuid(), shopId: req.params.shopId}))
        .then(defaultPutResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop/products/category/:categoryId список
 * @apiVersion 0.0.1
 * @apiGroup ShopsProducts
 * @apiUse ProductList
 */
router.get('/products/category/:categoryId', (req, res) => {
    Product.find({categoryId: req.params.categoryId})
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop/products/ список по идентификаторам
 * @apiVersion 0.0.1
 * @apiGroup ShopsProducts
 * @apiUse ProductList
 * @apiParamExample {json} идентификаторы товаров:
 *  { "ids": ["id1", "id2", "id3" ... ] }
 */
router.get('/products/', (req, res) => {
    Product.find({_id: {$in: req.body.ids}})
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {post} /shop/products/:productId обновление
 * @apiVersion 0.0.1
 * @apiGroup ShopsProducts
 * @apiUse ProductContent
 * @apiUse ProductInfo
 */
router.post('/products/:productId', (req, res) => {
    Product.update(Object.assign(req.body, {productId: req.params.productId}))
        .then(defaultPostResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {put} /shop/orders/:shopId создание
 * @apiVersion 0.0.1
 * @apiGroup ShopsOrders
 * @apiUse OrderContent
 * @apiUse OrderInfo
 */
router.put('/orders/:shopId', (req, res) => {
    Order.create(Object.assign(req.body, {_id: uuid(), shopId: req.params.shopId}))
        .then(defaultPutResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {get} /shop/orders/:userId список
 * @apiVersion 0.0.1
 * @apiGroup ShopsOrders
 * @apiUse OrderList
 */
router.get('/orders/:userId', (req, res) => {
    Order.find({userId: req.params.userId})
        .then(defaultResultResponse(res))
        .catch(defaultErrorResponse(res))
});

/**
 * @api {post} /shop/orders/:orderId обновление
 * @apiVersion 0.0.1
 * @apiGroup ShopsOrders
 * @apiUse OrderContent
 * @apiUse OrderInfo
 */
router.post('/orders/:orderId', (req, res) => {
    Order.update(Object.assign(req.body, {_id: req.params.orderId}))
        .then(defaultPostResponse(res))
        .catch(defaultErrorResponse(res))
});

module.exports = router;