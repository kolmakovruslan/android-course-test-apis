'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @apiDefine ShopId
 * @apiParam {String} _id идентификатор
 */
/**
 * @apiDefine ShopContent
 * @apiParam {String} name название
 * @apiParam {String} description описание
 * @apiParam {String} picture ссылка на картинку
 */
/**
 * @apiDefine ShopInfo
 * @apiSuccess {Object} shop информация о магазине
 */
/**
 * @apiDefine ShopList
 * @apiSuccess {Object[]} shops список магазинов
 */
const ShopSchema = new Schema({
    _id: {type: String, default: "NoId"},
    name: {type: String, default: "Empty name"},
    description: {type: String, default: "Empty description"},
    picture: {type: String, default: "Empty picture link"},
});

/**
 * @apiDefine UserId
 * @apiParam {String} _id идентификатор
 */
/**
 * @apiDefine UserContent
 * @apiParam {String} login логин
 * @apiParam {String} shopId идентификатор магазина
 * @apiParam {String} firstName имя
 * @apiParam {String} lastName фамилия
 * @apiParam {String} description описание
 * @apiParam {String} picture ссылка на картинку
 */
/**
 * @apiDefine UserInfo
 * @apiSuccess {Object} user информация о пользователе
 */
/**
 * @apiDefine UserList
 * @apiSuccess {Object[]} users список пользователей
 */
const UserSchema = new Schema({
    _id: {type: String, default: "NoId"},
    shopId: {type: String, default: "NoShopId"},
    login: {type: String, default: "Empty login"},
    fistName: {type: String, default: "Empty first name"},
    lastName: {type: String, default: "Empty last name"},
    description: {type: String, default: "Empty description"},
    picture: {type: String, default: "Empty picture link"},
});

/**
 * @apiDefine CategoryId
 * @apiParam {String} _id идентификатор
 */
/**
 * @apiDefine CategoryContent
 * @apiParam {String} shopId идентификатор магазина
 * @apiParam {String} name название
 * @apiParam {String} description описание
 * @apiParam {String} picture ссылка на картинку
 */
/**
 * @apiDefine CategoryInfo
 * @apiSuccess {Object} category информация о категории
 */
/**
 * @apiDefine CategoryList
 * @apiSuccess {Object[]} categories список категорий
 */
const CategorySchema = new Schema({
    _id: {type: String, default: "NoId"},
    shopId: {type: String, default: "NoShopId"},
    name: {type: String, default: "Empty name"},
    description: {type: String, default: "Empty description"},
    picture: {type: String, default: "Empty picture link"},
});


/**
 * @apiDefine ProductId
 * @apiParam {String} _id идентификатор
 */
/**
 * @apiDefine ProductContent
 * @apiParam {String} categoryId идентификатор категории
 * @apiParam {String} name название
 * @apiParam {Number} price цена
 * @apiParam {String} description описание
 * @apiParam {Object[]} properties список свойств
 */
/**
 * @apiDefine ProductInfo
 * @apiSuccess {Object} product информация о товаре
 */
/**
 * @apiDefine ProductList
 * @apiSuccess {Object[]} products список товаров
 */
const ProductSchema = new Schema({
    _id: {type: String, default: "NoId"},
    categoryId: {type: String, default: "Empty category id"},
    name: {type: String, default: "Empty name"},
    price: {type: Number, default: "Empty price"},
    description: {type: String, default: "Empty description"},
    properties: {type: [Object], default: []},
});

/**
 * @ApiDefine OrderId
 * @apiParam {String} _id идентификатор
 */
/**
 * @ApiDefine OrderContent
 * @apiParam {String} userId идентификатор пользователя
 * @apiParam {String} dareCreate дата создания
 * @apiParam {String} dateUpdate дата обновления
 * @apiParam {String} comment комментарий
 * @apiParam {String[]} products список товаров
 */
/**
 * @apiDefine OrderInfo
 * @apiSuccess {Object} order информация о заказе
 */
/**
 * @apiDefine OrderList
 * @apiSuccess {Object[]} orders список заказов
 */
const OrderSchema = new Schema({
    _id: {type: String, default: "NoId"},
    userId: {type: String, default: "NoUserId"},
    dateCreate: {type: Date, default: Date.UTC(1, 1)},
    dateUpdate: {type: Date, default: Date.UTC(1, 1)},
    comment: {type: String, default: "Empty comment"},
    products: {type: [String], default: []},
});

module.exports.ShopSchema = ShopSchema;
module.exports.UserSchema = UserSchema;
module.exports.CategorySchema = CategorySchema;
module.exports.ProductSchema = ProductSchema;
module.exports.OrderShcema = OrderSchema;