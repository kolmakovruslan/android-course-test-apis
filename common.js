'use strict';

module.exports.defaultResultResponse = function (response) {
    return function (value) {
        response.status(200).send(value)
    }
};

module.exports.defaultPutResponse = function (response) {
    return function (value) {
        response.status(201).send(value)
    }
};

module.exports.defaultPostResponse = function (response) {
    return function (value) {
        response.status(200).send(value)
    }
};

module.exports.defaultDeleteResponse = function (response) {
    return function (value) {
        response.status(200).send(value)
    }
};

module.exports.defaultErrorResponse = function (response) {
    return function (error) {
        console.log(`
----------error_log----------
${error}
----------error_log----------
`);
        response.status(500).send({
            "code": error.code,
            "message": error.message
        })
    }
};