'use strict';

const mongoose = require('mongoose');

const connection = mongoose.connect('mongodb://localhost:3000');

connection.then(() => {
    console.log("Connect to db successful");
}).catch((error) => {
    console.log("Connect to db error\n" + error.message);
});